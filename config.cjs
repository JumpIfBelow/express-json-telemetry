module.exports = {
    webserver: {
        http: 8000,
    },
    websocket: {
        /**
         * @type {(number|[number, number])[]}
         */
        ports: [
            [9000, 9100],
            9250,
            [9300, 9333],
        ],
    },
};
