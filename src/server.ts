import 'module-alias/register';

import express from 'express';
import cors from 'cors';
import { DefaultController } from './controller/default.controller';
import config from '../config.cjs';
import * as http from 'http';
import { WebSocketServer } from 'ws';
import { filter, map } from 'rxjs';
import { EventType } from '@enum/event-type.enum';
import { SocketProxyManagerService } from '@service/socket-proxy-manager.service';

const app = express();
const httpServer = http.createServer(app);
const websocketServer = new WebSocketServer({ server: httpServer });
app.use(cors());

SocketProxyManagerService
    .socketProxyEvent$
    .pipe(
        filter(webSocketEvent => webSocketEvent.event === EventType.CREATE),
        map(({ item }) => item),
    )
    .subscribe(webSocket => {
        const bindSocket$ = webSocket.socketServer.bindSocket();

        websocketServer.on('connection', ws => {
            webSocket.webSocketClients.push(ws);
            const subscription = bindSocket$.subscribe(b => ws.send(b));

            ws.on('open', () => webSocket.webSocketClients.push(ws));
            ws.on('message', rd => bindSocket$.next(rd as Buffer));
            ws.on('close', () => {
                subscription?.unsubscribe();
                webSocket.webSocketClients.splice(webSocket.webSocketClients.indexOf(ws), 1);
            });
        });

        httpServer.listen(
            webSocket.port,
            webSocket.host,
            () => console.info(`Listening websocket ${webSocket.getUri()} to ${webSocket.socketServer.datagram}://${webSocket.socketServer.host}:${webSocket.socketServer.port}.`),
        );
    })
;

SocketProxyManagerService
    .socketProxyEvent$
    .pipe(
        filter(webSocketEvent => webSocketEvent.event === EventType.DELETE),
        map(({ item }) => item),
    )
    .subscribe(webSocket => {
        webSocket.socketServer.unbindSocket();

        console.log(Array.from(websocketServer.clients).map(client => client.url));
        // TODO how to find the corresponding client and close the right one?
        // TODO link a Websocket object to all of its ws.Websocket?
        Array
            .from(websocketServer.clients)
            .find(client => client)
            ?.close()
        ;
    })
;

app.use('/', DefaultController);

const httpPort = config.webserver.http;
app.listen(httpPort, '0.0.0.0', () => console.log(`HTTP server listening to http://localhost:${httpPort}`));
