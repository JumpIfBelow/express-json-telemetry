import { Router } from "express";
import * as bodyParser from "body-parser";
import { Datagram } from "@enum/datagram.enum";
import { Socket } from "@model/socket.class";
import { HttpError } from "@model/http-error.class";
import { SocketProxyFactoryService } from "@service/socket-proxy-factory.service";
import { SocketProxyManagerService } from "@service/socket-proxy-manager.service";

const router = Router();

router
    .route('/socket-proxies')
    .post(bodyParser.json(), (req, res, next) => {
        const host: string = req.body.host;
        const port: number = req.body.port;
        const datagram: Datagram = req.body.datagram;

        const socket = new Socket(host, port, datagram);
        const socketProxy = SocketProxyFactoryService.createSocketProxy(req.socket.remoteAddress!, req.ip, socket);

        if (!socketProxy) {
            res
                .status(500)
                .write(new HttpError(500, 'Unable to create the websocket.'))
            ;

            next();
            return;
        }

        SocketProxyManagerService.add(socketProxy);

        res
            .status(201)
            .send(socketProxy.toJson())
        ;
        next();
    })
;

export const PostSocketProxyController = router;
