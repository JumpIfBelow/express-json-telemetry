import { Router } from "express";
import { HttpError } from "@model/http-error.class";
import { SocketProxyManagerService } from "@service/socket-proxy-manager.service";

const router = Router();

router
    .route('/socket-proxies')
    .get((_req, res, next) => {
        res.send(SocketProxyManagerService
            .getSocketProxies()
            .map(socketProxy => socketProxy.toJson()),
        );
        next();
    })
;

router
    .route('/socket-proxies/:port')
    .get((req, res, next) => {
        const port = parseInt(req.params.port);

        const socketProxy = SocketProxyManagerService
            .getSocketProxies()
            .find(socketProxy => socketProxy.port === port)
        ;

        if (!socketProxy) {
            res
                .status(404)
                .send(new HttpError(404, 'Not found.'))
            ;

            next();
            return;
        }

        res.send(socketProxy.toJson());
        next();
    })
;

export const GetSocketProxyController = router;
