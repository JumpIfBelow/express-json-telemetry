import { EventType } from "@enum/event-type.enum";

export interface ItemEvent<T> {
    event: EventType;
    item: T;
}
