import * as dgram from 'dgram';
import { Observable, Observer, Subject } from 'rxjs';

/**
 * Creates a UDP socket with the given URI.
 * @param host {string} A string representing the host to connect to
 * @param port {number} An integer representing the port to connect to
 * @param datagram {dgram.SocketType} The datagram to create the client
 * @returns {Subject<Buffer>} A new UDP socket client.
 */
export function udp(host: string, port: number, datagram: dgram.SocketType = 'udp4'): Subject<Buffer> {
    const client = dgram.createSocket(datagram);

    const observable = new Observable<Buffer>(
        subscriber => {
            client.on('message', data => subscriber.next(data));

            return () => {
                client.close();
            };
        },
    );

    const observer: Observer<Buffer> = {
        next: data => client.send(data, port, host),
        error: error => console.error(error),
        complete: () => console.info('UDP emission complete.'),
    };

    return Subject.create(observer, observable);
}
