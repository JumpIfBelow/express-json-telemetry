export enum EventType {
    CREATE = 'create',
    UPDATE = 'update',
    DELETE = 'delete',
}
